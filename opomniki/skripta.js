window.addEventListener("load", function() {
	// Stran naložena
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// TODO:
			if (cas == 0) {
				var naziv_opomnika = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev "+ naziv_opomnika + " je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
				
			}else {
				casovnik.innerHTML = cas-1;
			}
			// - če je čas enak 0, izpiši opozorilo
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	};
	
	setInterval(posodobiOpomnike, 1000);
	
	var izvediPrijavo = function() {
		var uporabnik = document.getElementById("uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML = uporabnik;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
		
	};
	var prijavniGumb = document.getElementById("prijavniGumb");
	prijavniGumb.addEventListener('click', izvediPrijavo);
	
	var dodajOpomnik = function() {
		var naziv_opomnika = document.getElementById("naziv_opomnika").value;
		var cas_opomnika = document.getElementById("cas_opomnika").value;
		document.getElementById("naziv_opomnika").value = '';
		document.getElementById("cas_opomnika").value = '';
		var opomniki = document.getElementById("opomniki");
		opomniki.innerHTML += " \
			<div class='opomnik senca rob'> \
			  <div class='naziv_opomnika'>" + naziv_opomnika + " </div> \
			  <div class='cas_opomnika'> Opomnik čez <span>" + cas_opomnika +" \
			  </span> sekund.</div> \
			</div>";
		
			
	};
	document.getElementById("dodajGumb").addEventListener('click', dodajOpomnik);
});


	